import { combineReducers } from 'redux';
import reducer from '../AdminDashboard/Action/reducer';

export default combineReducers({
  user: reducer,
});
