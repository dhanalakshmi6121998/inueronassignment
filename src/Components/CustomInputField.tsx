import React from 'react';

interface ICustomInputField {
  type: string;
  value: string;
  name: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  isRequired?: boolean;
  placeholder?: string;
}

function CustomInputField(props: ICustomInputField) {
  const { type, value, name, onChange, isRequired, placeholder } = props || {};
  return (
    <input
      className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5'
      id={name}
      type={type}
      value={value}
      name={name}
      onChange={onChange}
      required={isRequired}
      placeholder={placeholder}
    />
  );
}

export default CustomInputField;
