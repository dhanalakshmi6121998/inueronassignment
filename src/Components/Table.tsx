import React from 'react';
interface Iheader {
  label: string;
  key: string;
}

function Table(props: any) {
  const { data, onDelete, onEdit } = props || {};

  const header = [
    {
      label: 'firstName',
      key: 'firstName',
    },
    {
      label: 'lastName',
      key: 'lastName',
    },
    {
      label: 'phoneNumber',
      key: 'phoneNumber',
    },
    {
      label: 'age',
      key: 'age',
    },
    {
      label: 'Edit',
      key: 'Edit',
    },
    {
      label: 'Delete',
      key: 'Delete',
    },
  ];
  return (
    <table className='table-fixed border border-slate-600'>
      <thead>
        <tr>
          {header.map((item: Iheader) => (
            <th className='border text-left px-8 py-4 ' key={item.key}>
              {item.label}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.length ? (
          data.map((item: any) => {
            return (
              <tr>
                <td className='border px-8 py-4'>{item.firstName}</td>
                <td className='border px-8 py-4'>{item.lastName}</td>
                <td className='border px-8 py-4'>{item.phoneNumber}</td>
                <td className='border px-8 py-4'>{item.age}</td>

                <td className='border px-8 py-4'>
                  <button onClick={() => onEdit(item._id)} className={'cursor-pointer'}>
                    Edit
                  </button>
                </td>
                <td className='border px-8 py-4'>
                  <button onClick={() => onDelete(item._id)} className={'cursor-pointer'}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })
        ) : (
          <tr>
            <td className='border px-8 py-4'>No Data</td>
          </tr>
        )}
      </tbody>
    </table>
  );
}

export default Table;
