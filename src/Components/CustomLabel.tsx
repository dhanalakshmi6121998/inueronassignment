import React from 'react';

interface ILabel {
  label: string;
}

function CustomLabel(props: ILabel) {
  const { label } = props || {};

  return <label className='block text-gray-700 text-sm font-bold mb-2'>{label}</label>;
}

export default CustomLabel;
