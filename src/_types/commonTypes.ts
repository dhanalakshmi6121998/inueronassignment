export interface IUserDetails {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  age: number;
  _id?: string;
}
