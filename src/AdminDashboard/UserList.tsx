import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Table from '../Components/Table';
import {
  getUserDetails,
  addUser,
  deleteUserDetails,
  getUserDetailsById,
  editUserDetails,
} from './Action/action';
import UserDetails from './UserDetails';

function UserList(props: any) {
  const users = useSelector((store: any) => store.user);
  const { selectedUser = {} } = users || {};
  const [userData, setUSerData] = useState<any>({});
  const [error, setError] = useState({});
  const dispatch = useDispatch();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target || {};
    setUSerData({
      ...userData,
      [name]: value,
    });
  };

  const handleAdd = () => {
    const error = validateForm();
    if (Object.keys(error)?.length) {
      setError(error);
    }
    if (!userData._id && !Object.keys(error)?.length) {
      dispatch(addUser(userData));
      setUSerData({});
    } else if (userData._id && !Object.keys(error)?.length) {
      dispatch(editUserDetails(userData));
      setUSerData({});
    }
  };

  const handleEdit = (id: string) => {
    dispatch(getUserDetailsById(id));
  };

  const handleDelete = (id: string) => {
    dispatch(deleteUserDetails(id));
  };

  const validateForm = () => {
    const { firstName = '', lastName = '', phoneNumber = '', age = 0 } = userData || {};
    let error = {};
    if (!firstName) {
      error = {
        ...error,
        ['firstName']: {
          isError: true,
          message: 'Please Enter First Name',
        },
      };
    } else if (!lastName) {
      error = {
        ...error,
        ['lastName']: {
          isError: true,
          message: 'Please Enter Last Name',
        },
      };
    } else if (!phoneNumber) {
      error = {
        ...error,
        ['phoneNumber']: {
          isError: true,
          message: 'Please Enter Phone Number',
        },
      };
    } else if (!age) {
      error = {
        ...error,
        ['age']: {
          isError: true,
          message: 'Please Enter valid age',
        },
      };
    }
    return error;
  };

  useEffect(() => {
    setUSerData(selectedUser);
  }, [selectedUser]);

  useEffect(() => {
    dispatch(getUserDetails());
  }, []);

  return (
    <div className='flex items-center justify-center flex-col h-screen'>
      <UserDetails
        UserDetails={userData}
        onChange={handleChange}
        buttonLabel={userData._id ? 'Edit User' : 'Add User'}
        onAction={handleAdd}
        error={error}
      />
      <Table data={users.userList || []} onDelete={handleDelete} onEdit={handleEdit} />
    </div>
  );
}

export default UserList;
