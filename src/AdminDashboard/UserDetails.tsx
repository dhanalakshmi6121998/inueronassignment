import React from 'react';
import CustomInputField from '../Components/CustomInputField';
import CustomLabel from '../Components/CustomLabel';

function UserDetails(props: any) {
  const { UserDetails, onChange, buttonLabel, onAction, error } = props || {};
  const { firstName = '', lastName = '', phoneNumber = '', age = 0 } = UserDetails || {};

  return (
    <div className='w-full max-w-xs'>
      <form className=' px-8 pt-6 pb-8 mb-4'>
        <div className='mb-4'>
          <CustomLabel label='First Name' />
          <CustomInputField
            type='text'
            value={firstName}
            name='firstName'
            onChange={onChange}
            placeholder='First Name'
          />
          {Object.keys(error)?.length !== 0 && error['firstName']?.isError && (
            <p className='text-red-500 text-xs italic'>{error['firstName']?.message}</p>
          )}
        </div>
        <div className='mb-4'>
          <CustomLabel label='Last Name' />
          <CustomInputField
            type='text'
            value={lastName}
            name='lastName'
            onChange={onChange}
            placeholder='Last Name'
          />
          {Object.keys(error)?.length !== 0 && error['lastName']?.isError && (
            <p className='text-red-500 text-xs italic'>{error['lastName']?.message}</p>
          )}
        </div>
        <div className='mb-4'>
          <CustomLabel label='Phone Number' />
          <CustomInputField
            type='text'
            value={phoneNumber}
            name='phoneNumber'
            onChange={onChange}
            placeholder='Phone Number'
          />
          {Object.keys(error)?.length !== 0 && error['phoneNumber']?.isError && (
            <p className='text-red-500 text-xs italic'>{error['phoneNumber']?.message}</p>
          )}
        </div>
        <div className='mb-4'>
          <CustomLabel label='Age' />
          <CustomInputField
            type='number'
            value={age}
            name='age'
            onChange={onChange}
            placeholder='Age'
          />
          {Object.keys(error)?.length !== 0 && error['age']?.isError && (
            <p className='text-red-500 text-xs italic'>{error['age']?.message}</p>
          )}
        </div>

        <div className='md:flex md:items-center'>
          <button
            className='shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded'
            type='button'
            onClick={onAction}
          >
            {buttonLabel}
          </button>
        </div>
      </form>
    </div>
  );
}

export default UserDetails;
