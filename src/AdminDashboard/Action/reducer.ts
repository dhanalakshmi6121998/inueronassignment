import Types from './type';

const initialState = {
  loading: false,
  userList: [],
  selectedUser: {},
  error: '',
};

const userReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case Types.GET_USER:
      return {
        ...state,
        error: '',
        userList: action.payload,
      };
    case Types.GET_USER_BY_ID:
      return {
        ...state,
        error: '',
        selectedUser: action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;
