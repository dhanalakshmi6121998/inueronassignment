import Types from './type';
import axios from 'axios';
import { IUserDetails } from '../../_types/commonTypes';

const getUSer = (users: IUserDetails[]) => ({
  type: Types.GET_USER,
  payload: users,
});

const getUSerById = (user: IUserDetails) => ({
  type: Types.GET_USER_BY_ID,
  payload: user,
});

const ApiConstant: any = {
  hostname: 'https://blue-journalist-bbrpv.ineuron.app:4000',

  endpoints: {
    user: '/users',
    createUser: '/user/create',
    userbyId: '/user/',
  },
};
export default ApiConstant;

export const getUserDetails = (): any => {
  return function (dispatch: any) {
    axios
      .get(`${ApiConstant.hostname}/users`)
      .then((res: any) => {
        dispatch(getUSer(res.data.data));
      })
      .catch((error: any) => console.log(error));
  };
};

export const addUser = (payload: any): any => {
  return function (dispatch: any) {
    axios
      .post(`${ApiConstant.hostname}${ApiConstant.endpoints.createUser}`, payload)
      .then((res: any) => {
        dispatch(getUserDetails());
      })
      .catch((error: any) => console.log(error));
  };
};

export const deleteUserDetails = (id: string): any => {
  return function (dispatch: any) {
    axios
      .delete(`${ApiConstant.hostname}${ApiConstant.endpoints.userbyId}${id}`)
      .then((res: any) => {
        dispatch(getUserDetails());
      })
      .catch((error: any) => console.log(error));
  };
};

export const getUserDetailsById = (id: string): any => {
  return function (dispatch: any) {
    axios
      .get(`${ApiConstant.hostname}${ApiConstant.endpoints.userbyId}${id}`)
      .then((res: any) => {
        dispatch(getUSerById(res.data.data));
      })
      .catch((error: any) => console.log(error));
  };
};

export const editUserDetails = (userDetails: IUserDetails): any => {
  const { _id } = userDetails || {};
  return function (dispatch: any) {
    axios
      .patch(`${ApiConstant.hostname}${ApiConstant.endpoints.userbyId}${_id}`, userDetails)
      .then((res: any) => {
        dispatch(getUserDetails());
      })
      .catch((error: any) => console.log(error));
  };
};
